# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import requests
import json
import time

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
  #  print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.

    print("*******OBTIENE TOKEN*********")

    url = "https://api.softwareavanzado.world/index.php?option=token&api=oauth2"

    payload = 'grant_type=client_credentials&client_id=sa&client_secret=fb5089840031449f1a4bf2c91c2bd2261d5b2f122bd8754ffe23be17b107b8eb103b441de3771745'
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cookie': '__cfduid=d84c217dc47433a07667a785d6dba94ef1597449566; 1bb11e6f2dacb1c375d150942d6da0cd=bjjtmbqkch3g7ltis0fmf39rs5'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)
    r= json.loads(response.text)
    token = r["access_token"]
    time.sleep(2)

    #Lista contactos

    url = "https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal"

    payload = {}
    bearer_token= "Bearer "+token
    headers = {
        'Authorization': bearer_token,
        'Cookie': '__cfduid=d84c217dc47433a07667a785d6dba94ef1597449566; 1bb11e6f2dacb1c375d150942d6da0cd=bjjtmbqkch3g7ltis0fmf39rs5'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    print("\n*******LISTADO DE CONTACTOS*********")
    print(response.text.encode('utf8'))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
