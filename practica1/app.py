from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import requests

app = Flask(__name__)
api = Api(app)


@app.route('/')
def hello_world():
    return 'Servidor inicializado'


class postContacto(Resource):
    def post(self):
        # hacemosel POST
        contacto = {"name" : "Kenia Zepeda"}
        response = requests.post('https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal', data=contacto)
        print(response.text)
        return response.json()


class getContactos(Resource):
    def get(self):
        # hacemos el GET
        response = requests.get('https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal')
        print("Lista de contactos: \n")
        print(response.text)
        return response.json()

api.add_resource(postContacto, '/postContacto')
api.add_resource(getContactos, '/getContactos')

if __name__ == '__main__':
     app.run(port='5000')