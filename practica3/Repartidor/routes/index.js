var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//Recibir pedido del restaurante
router.post('/recibirPedido', function(req, res) {
  console.log("\n************Recibir pedido del restaurante***********************")
  console.log(JSON.stringify(req.body))
  console.log("****************************************************")
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Pedido recibido',
  };
  res.send(respuesta);
});

//Informar estado del pedido al cliente
router.get('/estadoPedido', function(req, res) {
  console.log("\n************Informar estado del pedido al cliente***********************")
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Pedido en proceso',
  };
  res.send(respuesta);
});


//Marcar como entregado
router.get('/marcarEntregado', function(req, res) {
  console.log("\n************Marcar como entregado***********************")
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Pedido entregado',
  };
  res.send(respuesta);
});

module.exports = router;
