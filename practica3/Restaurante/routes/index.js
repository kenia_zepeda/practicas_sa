var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//Recibir pedido del cliente
router.post('/recibirPedido', function(req, res) {
  console.log("\n************Recibir pedido del cliente***********************")
  console.log(JSON.stringify(req.body))
  console.log("****************************************************")
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Pedido recibido',
  };
  res.send(respuesta);
});

//Informar estado del pedido al cliente
router.get('/estadoPedido', function(req, res) {
  console.log("\n************Informar estado del pedido al cliente***********************")
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Pedido en proceso',
  };
  res.send(respuesta);
});


//Avisar al repartidor que ya está listo el pedido
var request = require('request');
var options = {
  'method': 'POST',
  'url': 'http://localhost:3001/recibirPedido',
  'headers': {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({"cliente":"Albert Newton","orden":"Quesoburguesa doble","total":"35.00"})

};
request(options, function (error, response) {
  if (error) throw new Error(error);
  console.log("\n************Avisar al repartidor que ya está listo el pedido***********************")
  console.log(response.body);
});

module.exports = router;
