var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});



//Solicitar pedido al restaurante
var request = require('request');
var options = {
  'method': 'POST',
  'url': 'http://localhost:3002/recibirPedido',
  'headers': {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({"cliente":"Albert Newton","orden":"Quesoburguesa doble","total":"35.00"})

};
request(options, function (error, response) {
  if (error) throw new Error(error);
  console.log("\n************Solicitar pedido al restaurante***********************")
  console.log(response.body);
});



//Verificar estado del pedido al restaurante
var request = require('request');
var options = {
  'method': 'GET',
  'url': 'http://localhost:3002/estadoPedido',
  'headers': {
    'Content-Type': 'application/json'
  }
};
request(options, function (error, response) {
  if (error) throw new Error(error);
  console.log("\n************Verificar estado del pedido al restaurante***********************")
  console.log(response.body);
});



//Verificar estado del pedido al repartidor
var request = require('request');
var options = {
  'method': 'GET',
  'url': 'http://localhost:3001/estadoPedido',
  'headers': {
    'Content-Type': 'application/json'
  }
};
request(options, function (error, response) {
  if (error) throw new Error(error);
  console.log("\n************Verificar estado del pedido al repartidor***********************")
  console.log(response.body);
});


module.exports = router;